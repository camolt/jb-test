import environment_setup
import os
from flask import Flask, render_template
from v1 import bind_api

app = Flask(__name__,
            static_url_path="/dist",
            static_folder="../client/dist",
            template_folder="../client/build")

bind_api(app)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin',
                         os.environ.get('ORIGIN', 'http://127.0.0.1:8080'))
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,X-JWT-Token')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response

@app.route('/', defaults={'path': ''}, methods=['GET'])
@app.route('/login', defaults={'path': '/login'}, methods=['GET'])

def catch_all(path):
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
