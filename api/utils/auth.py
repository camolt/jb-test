from functools import wraps
from flask import request, jsonify, redirect
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import SignatureExpired, BadSignature
from os import environ
import logging

TWO_WEEKS = 1209600

def generate_token(user, expiration=TWO_WEEKS):
    logging.debug('User: %s' % user)
    s = Serializer(environ.get("SECRET_KEY"), expires_in=expiration)
    token = s.dumps(user).decode('utf-8')
    logging.debug('Token: %s' % token)
    return token


def verify_token(token):
    s = Serializer(environ.get("SECRET_KEY"))
    try:
        data = s.loads(token)
    except (BadSignature, SignatureExpired):
        return None
    return data


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('Authorization', None)
        if token:
            string_token = token.encode('ascii', 'ignore')
            user = verify_token(string_token)
            if user:
                g.current_user = user
                return f(*args, **kwargs)

        return redirect('/login')

    return decorated
