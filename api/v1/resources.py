from flask import jsonify, request
from flask_restful import Resource, Api
from utils.auth import generate_token, verify_token
from zeep import Client, helpers

API_PREFIX = '/api/v1'

def bind_api(app):
    api = Api(app)
    api.add_resource(GetToken, API_PREFIX + '/get_token')
    api.add_resource(CheckVat, API_PREFIX + '/check_vat')
    return api


class GetToken(Resource):
    def post(self):
        incoming = request.form
        incomingJson = dict(incoming)
        if incoming['login'] == "info@jbsolutions.pl" and incoming['password'] == "jbsolutions":
            return jsonify(token=generate_token(incomingJson))

        return jsonify(error=True), 403


class CheckVat(Resource):
    def post(self):
        print(request.form['nip'])
        incoming = request.form
        client = Client('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl')
        result = client.service.checkVat('PL', incoming['nip'])
        print("valid", result)
        return jsonify(helpers.serialize_object(result))
