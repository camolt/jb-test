const { resolve } = require('path');

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

// ENV variables
// Original: https://github.com/auth0-samples/auth0-react-sample/tree/master/02-Custom-Login
const envVariables = require('dotenv').config({path: '../.env'});
const envDefines = Object.keys(envVariables.parsed).reduce((memo, key) => {
    const val = JSON.stringify(envVariables.parsed[key]);
    memo[`__${key.toUpperCase()}__`] = val;
    return memo;
}, {});
// END ENV variables

const config = {
    devtool: 'cheap-module-eval-source-map',

    entry: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://127.0.0.1:8080',
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        './main.js'
        //'./assets/scss/main.scss',
    ],

    output: {
        filename: 'bundle.js',
        path: resolve(__dirname, 'dist'),
        publicPath: '/dist'
    },

    context: resolve(__dirname, 'src'),

    devServer: {
        hot: true,
        contentBase: resolve(__dirname, 'build'),
        publicPath: '/dist',
        historyApiFallback: true
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loaders: [
                    'babel-loader',
                ],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            query: {
                                sourceMap: false,
                            },
                        },
                    ],
                }),
            },
            { test: /\.(png|jpg)$/, use: 'url-loader?limit=15000' },
            { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
            { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' },
        ]
    },

    plugins: [
        new webpack.LoaderOptionsPlugin({
            test: /\.js$/,
            options: {
                eslint: {
                    configFile: resolve(__dirname, '.eslintrc'),
                    cache: false,
                }
            },
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new webpack.DefinePlugin(envDefines),
        new ExtractTextPlugin({ filename: 'style.css', disable: true, allChunks: true }),
        new CopyWebpackPlugin([{ from: 'vendors', to: 'vendors' }]),
        new webpack.HotModuleReplacementPlugin(),
        // Webpack dashboard plugin
        new DashboardPlugin()
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    },
};

module.exports = config;
