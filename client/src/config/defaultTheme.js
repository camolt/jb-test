import colors from './variables';

export default {
    fontFamily: 'Source Sans Pro',
    menuItem: {
        hoverColor: colors.selection,
        selectedTextColor: colors.selection,
    },
    palette: {
        primary1Color: colors.selection,
        textColor: '#000',
        secondaryTextColor: '#000',
        alternateTextColor: '#000',
        cavasColor: colors.lighterBlue,
    },
    listItem: {
        leftIconColor: colors.lighterBlue,
        rightIconColor: colors.lighterBlue,
    },
    button: {
        height: 41,
      // minWidth: 88,
      // iconButtonSize: spacing.iconSize * 2
    },
    svgIcon: {
        color: colors.lighterBlue,
    },
    textField: {
        textColor: colors.darkBlue,
        // hintColor: 'orange',
        // floatingLabelColor: y
        // disabledTextColor: 'orange',
        // errorColor: 'orange',
        focusColor: colors.selection,
        borderColor: colors.lighterBlue,
    },
    raisedButton: {
        primaryColor: colors.blue,
        primaryTextColor: colors.lighterBlue,
    },
    dropDownMenu: {
        accentColor: colors.selection,
    },
};
