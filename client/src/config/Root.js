import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
// If you want to start the app from file system:
// import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider, observer } from 'mobx-react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import AppState from '../stores/AppState';
import AuthService from '../stores/AuthService';
import LoginPage from '../components/Login/Login';
import * as pages from '../pages';


@observer
export default class Root extends Component {
    constructor(props) {
        super(props);
        // TODO: Probably better way to use several stores (almost same)
        // https://github.com/mhaagens/react-mobx-react-router4-boilerplate/issues/29#issuecomment-289713335
        this.auth = AuthService;
        this.store = new AppState(AuthService);
        injectTapEventPlugin();
    }

    render() {
        return (
            <Provider store={this.store} auth={this.auth}>
              <Router>
                <Switch>
                  <Route exact path="/" component={() => <Redirect to="/vatquery" />} />
                  <Route path="/history" component={ pages.HistoryPage } />
                  <Route path="/vatquery" component={ pages.VatQueryPage } />
                  <Route exact path="/login" component={ LoginPage } />
                </Switch>
              </Router>
            </Provider>
        );
    }
}
