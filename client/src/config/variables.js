const colors = {
    darkBlue: '#07192d',
    lighterBlue: '#d5e0f0',
    selection: '#134379',
    blue: '#378df0',
    background: '#f7fafc',
    gray: '#7e8894',
    headerText: '#919AA4',
    text: '#6D7F94',
};

export default colors;
