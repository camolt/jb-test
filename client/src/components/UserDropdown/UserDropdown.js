import React from 'react';
import PropTypes from 'prop-types';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import ExpandIcon from 'material-ui/svg-icons/navigation/expand-more';
import LessIcon from 'material-ui/svg-icons/navigation/expand-less';
import ExitIcon from 'material-ui/svg-icons/action/power-settings-new';
import HelpIcon from 'material-ui/svg-icons/action/help';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import DropdownLink from './DropdownLink';
import UserInfo from '../UserInfo/UserInfo';


const UserDropdown = ({ showSettings, opened, logout }) => {
    return (
        <div className="user-info">
            <UserInfo name="Jbsolutions User" />
            <DropDownMenu
              className="settings-dropdown"
              value={'value'}
              autoWidth
              labelStyle={{ width: '250px' }}
              underlineStyle={{ display: 'none' }}
              targetOrigin={{ vertical: 'top', horizontal: 'right' }}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              iconButton={opened ? <LessIcon /> : <ExpandIcon />}
              iconStyle={{ fill: 'black', right: 0 }}
              onClose={() => showSettings(false)}
              onClick={() => showSettings(true)}
            >
                <DropdownLink
                  key="Settings"
                  to="/settings"
                  primaryText="Settings"
                  leftIcon={<SettingsIcon color="black" style={{ width: '14px', margin: 0, top: '4px', left: '24px' }} />}
                />
            <MenuItem innerDivStyle={{ paddingLeft: '50px' }} primaryText="Help" leftIcon={<HelpIcon color="black" style={{ width: '14px' }} />} />
                <MenuItem innerDivStyle={{ paddingLeft: '50px' }} primaryText="Log Out" leftIcon={<ExitIcon color="black" style={{ width: '14px' }} />} onClick={logout} />
            </DropDownMenu>
        </div>
    );
};

UserDropdown.propTypes = {
    showSettings: PropTypes.func.isRequired,
    opened: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
};

export default UserDropdown;
