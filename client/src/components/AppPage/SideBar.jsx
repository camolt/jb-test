import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import Drawer from 'material-ui/Drawer';
import Paper from 'material-ui/Paper';
import muiThemeable from 'material-ui/styles/muiThemeable';

const getWidth = width => (typeof width === 'number' ? `${width}px` : width);

const getStyles = ({ drawer }) => {
    const width = '260px';
    const hideWidth = '230px';

    return ({
        sidebarOpen: {
            flex: `0 0 ${width}`,
            marginLeft: 0,
            order: -1,
            transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
        sidebarClosed: {
            flex: `0 0 ${width}`,
            marginLeft: `-${hideWidth}`,
            order: -1,
            transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        },
    });
};

// We shouldn't need PureComponent here as it's connected
// but for some reason it keeps rendering even though mapStateToProps returns the same object
class SideBar extends PureComponent {
    handleClose = () => {
        this.props.setSidebarVisibility(false);
    }

    render() {
        const { open, setSidebarVisibility, children, muiTheme } = this.props;
        const styles = getStyles(muiTheme);

        return (
            <Paper style={open ? styles.sidebarOpen : styles.sidebarClosed}>
              {children}
            </Paper>
        );
    }
}

SideBar.propTypes = {
    children: PropTypes.node.isRequired,
    muiTheme: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    setSidebarVisibility: PropTypes.func.isRequired,
};

export default compose(
    muiThemeable(),
)(SideBar);
