import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import UserDropdown from '../UserDropdown/UserDropdown';
import colors from '../../config/variables';

const AppBar = ({ title, toggleSidebar, userLogout, showSettings, opened, handleSubmit, onNipChange, searchBar }) => (
    <div className="app-bar">
        { searchBar ?
        <div>
            <form onSubmit={(event) => handleSubmit(event)}>
                <IconButton type="submit" className="search-icon" style={{ position: 'absolute', top: 0, left: 18, width: '50px', height: '50px', marginLeft: 0, margin: 'auto', bottom: 0 }}>
                    <SearchIcon color={colors.blue} />
                </IconButton>
                <TextField
                    style={{ height: 69, top: 6 }}
                    inputStyle={{ padding: '0 5em 0 4em' }}
                    hintText="Check VAT EU number for Poland"
                    hintStyle={{ padding: '0 4em', lineHeight: '50px', color: colors.gray }}
                    fullWidth
                    onChange={onNipChange}
                    name="nip"
                    className="text-field"
                    />
            </form>
        </div> :
        <div className='top-bar' /> }
        <UserDropdown showSettings={showSettings} opened={opened} logout={userLogout} />
    </div>
);

AppBar.propTypes = {
    userLogout: PropTypes.func,
    title: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
    ]).isRequired,
    toggleSidebar: PropTypes.func.isRequired,
    showSettings: PropTypes.func.isRequired,
};

export default AppBar;
