import React from 'react';
import PropTypes from 'prop-types';
import { CardTitle } from 'material-ui/Card';
import withWidth from 'material-ui/utils/withWidth';
import AppBarMobile from './AppBarMobile';

const ViewTitle = ({ title, width, toggleSidebar }) => (
    width === 1
        ? <AppBarMobile title={title} toggleSidebar={toggleSidebar} />
    : <CardTitle title={title} className="title" style={{ padding: '12px 0' }} />
);

ViewTitle.propTypes = {
    title: PropTypes.node,
    width: PropTypes.number,
    toggleSidebar: PropTypes.func,
};


export default withWidth()(ViewTitle);
