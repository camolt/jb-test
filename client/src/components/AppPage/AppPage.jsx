import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { NavLink, Redirect, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import CircularProgress from 'material-ui/CircularProgress';
import IconButton from 'material-ui/IconButton';
import autoprefixer from 'material-ui/utils/autoprefixer';
import withWidth from 'material-ui/utils/withWidth';
import compose from 'recompose/compose';
import AppBar from './AppBar';
import SideBar from './SideBar';
import MuiAppBar from 'material-ui/AppBar';
import SidebarMenu from './SidebarMenu';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import defaultTheme from '../../config/defaultTheme';
import colors from '../../config/variables';

@inject('store')
@observer
class AppPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            settings: false,
        };
    }

    toggleMenu = () => {
        this.props.store.toggleMenu();
    }

    setSidebarVisibility = (value) => {
        this.props.store.setMenuVisibility(value);
    }

    showSettings = (value) => {
        this.setState({
            settings: value,
        });
    }

    render() {
        const styles = {
            wrapper: {
                // Avoid IE bug with Flexbox, see #467
                display: 'flex',
                flexDirection: 'column',
            },
            main: {
                display: 'flex',
                flexDirection: 'column',
                minHeight: '100vh',
            },
            body: {
                backgroundColor: '#fff',
                display: 'flex',
                flex: 1,
                height: '100vh',
                overflow: 'hidden',
            },
            bodySmall: {
                backgroundColor: colors.darkBlue,
                overflowY: 'hidden',
            },
            content: {
                flex: 1,
                padding: '2em',
                backgroundColor: colors.background,
                overflow: 'auto',
            },
            contentSmall: {
                flex: 1,
                paddingTop: '3em',
            },
            loader: {
                position: 'absolute',
                top: 0,
                right: 0,
                margin: 16,
                zIndex: 1200,
            },
            header: {
                display: 'flex',
                paddingLeft: 30,
                paddingTop: 18,
                backgroundColor: colors.darkBlue,
                justifyContent: 'space-between',
                borderBottom: `1px solid ${colors.selection}`,
            },
            logo: {
                maxWidth: 183,
                display: 'inline-block',
            },
            menuButton: {
                padding: 0,
                position: 'relative',
                top: -15,
            },
        };

        const { children, isLoading, width, store, searchBar, handleSubmit, nipChanged } = this.props;
        const muiTheme = getMuiTheme(defaultTheme);

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div style={styles.wrapper}>
                    <div style={styles.main}>
                        { /* isLoading && <CircularProgress
                            className="app-loader"
                            color="#fff"
                            size={30}
                            thickness={2}
                            style={styles.loader}
                            /> */ }
                        <div className="body" style={width === 1 ? styles.bodySmall : styles.body}>
                            <SideBar open={store.menuOpen} setSidebarVisibility={this.setSidebarVisibility}>
                                <div style={styles.header}>
                                    <div style={styles.logo}>
                                    </div>
                                    <IconButton style={styles.menuButton} onTouchTap={this.toggleMenu}>
                                        <MenuIcon />
                                    </IconButton>
                                </div>
                                <SidebarMenu />
                            </SideBar>
                            <div className="main">
                                <AppBar searchBar={searchBar} title="" handleSubmit={handleSubmit} onNipChange={nipChanged} toggleSidebar={this.toggleMenu} userLogout={store.auth.logout} opened={this.state.settings} showSettings={this.showSettings}/>
                                <div style={width === 1 ? styles.contentSmall : styles.content} className="content">
                                    {children}
                                </div>
                            </div>
                            {!store.auth.loggedIn && <Redirect to={{ pathname: '/login', state: { from: '/' } }} />}
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
    }

// React.PropTypes has moved into a different package since React v15.5. Please use the prop-types library instead.
AppPage.propTypes = {
    children: PropTypes.node.isRequired,
    isLoading: PropTypes.bool,
    width: PropTypes.number,
    searchBar: PropTypes.bool,
};

AppPage.defaultProps = {
    isLoading: true,
    searchBar: false,
};

const enhance = compose(
        withWidth(),
        withRouter,
    );

export default enhance(AppPage);
