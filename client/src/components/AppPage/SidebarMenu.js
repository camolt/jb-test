import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';
import compose from 'recompose/compose';
import MicIcon from 'material-ui/svg-icons/av/mic';
import Menu from 'material-ui/Menu';
import colors from '../../config/variables';
import MenuItemLink from './MenuItemLink';


const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        height: '100%',
        backgroundColor: colors.darkBlue,
        backgroundPosition: '0 84%',
        backgroundRepeat: 'no-repeat'
    }
};

const SidebarMenu = () => {

    return (
        <div style={styles.main}>
            <Menu>
                <MenuItemLink
                  key="Vat Query"
                  to="/vatquery"
                  primaryText="Vat Query" />
                <MenuItemLink
                  key="Query History"
                  to="/history"
                  primaryText="Query History" />
            </Menu>
        </div>
    );
};

SidebarMenu.propTypes = {
    // onMenuTap: PropTypes.func,
    // userLogout: PropTypes.func,
};

const enhance = compose(
    pure,
);

export default enhance(SidebarMenu);
