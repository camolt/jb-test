import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from 'material-ui/Snackbar';

function getStyles(context) {
    if (!context) return { primary1Color: '#00bcd4', accent1Color: '#ff4081' };
    const {
      muiTheme: {
        baseTheme: {
          palette: {
              primary1Color,
              accent1Color,
          },
        },
      },
    } = context;
    return { primary1Color, accent1Color };
}

export default class Notification extends React.Component {
    /* handleRequestClose = () => {
        this.props.hideNotification();
    }; */

    render() {
        const style = {};
        const { primary1Color, accent1Color } = getStyles(this.context);
        const { type, message } = this.props;
        if (type === 'warning') {
            style.backgroundColor = accent1Color;
        }
        if (type === 'confirm') {
            style.backgroundColor = primary1Color;
        }
        return (<Snackbar
          open={!!message}
          message={message}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
          bodyStyle={style}
        />);
    }
}

Notification.propTypes = {
    message: PropTypes.string,
    type: PropTypes.string,
    // hideNotification: PropTypes.func.isRequired,
};

Notification.contextTypes = {
    muiTheme: PropTypes.object.isRequired,
};
