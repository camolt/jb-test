import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import MenuItem from 'material-ui/MenuItem';
import { withRouter } from 'react-router-dom';

import colors from '../../config/variables';

@inject('store')
@observer
export class MenuItemLinkComponent extends Component {

    handleMenuTap = () => {
        this.props.history.push(this.props.to);
        this.props.store.setActiveLink(this.props.to);
    }

    render() {
        const { key, to, primaryText, leftIcon, store } = this.props; // eslint-disable-line

        return (
            <MenuItem
              style={{ color: colors.lighterBlue }}
              className={store.activeLink === to ? 'active-link' : ''}
              key={key}
              to={to}
              primaryText={primaryText}
              leftIcon={leftIcon}
              onTouchTap={this.handleMenuTap}
            />
        );
    }
}

MenuItemLinkComponent.propTypes = {
    history: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
    // onTouchTap: PropTypes.func.isRequired,
    to: PropTypes.string.isRequired,
    store: PropTypes.shape({
        toggleMenu: PropTypes.func,
        setActiveLink: PropTypes.func,
    }),
};

export default withRouter(MenuItemLinkComponent);
