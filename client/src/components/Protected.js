import React from 'react';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

/*
* Other way to implement protected page on route declaration is
* to create `MatchWhenAuthorized` component:
* https://github.com/ReactTraining/react-router/blob/4f93e9b1abd2768a7370c40120c0c1ab863a2c1c/website/examples/Auth.js#L60
*/

export default function Protected(Component) {
    @inject('auth') @observer
    class AuthenticatedComponent extends Component {
        render() {
            const { loggedIn } = this.props.auth;
            return (
                <div className="authComponent">
                    { loggedIn ? <Component {...this.props} /> : <Redirect to={{ pathname: '/login', state: { from: '/' } }} /> }
                </div>
            );
        }
    }

    AuthenticatedComponent.propTypes = {
        auth: PropTypes.shape({
            loggedIn: PropTypes.func,
        }),
    };

    return AuthenticatedComponent;
}
