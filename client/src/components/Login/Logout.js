import React from 'react';
import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import MenuItem from 'material-ui/MenuItem';
import ExitIcon from 'material-ui/svg-icons/action/power-settings-new';
import IconButton from 'material-ui/IconButton';
import colors from '../../config/variables';
// import { userLogout as userLogoutAction } from '../../actions/authActions';


const Logout = ({ userLogout }) => (
    <IconButton onClick={userLogout} style={{ width: '50px', height: '50px', padding: 0, verticalAlign: 'middle' }}>
        <ExitIcon color={colors.blue} />
    </IconButton>
);

Logout.propTypes = {
    userLogout: PropTypes.func,
};

export default Logout;
