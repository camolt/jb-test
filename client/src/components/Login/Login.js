import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router-dom';

// import axios from 'axios';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { Card, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import CircularProgress from 'material-ui/CircularProgress';
import LockIcon from 'material-ui/svg-icons/action/lock-outline';

import defaultTheme from '../../config/defaultTheme';
import Notification from '../AppPage/Notification';
import colors from '../../config/variables';

const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
    },
    card: {
        minWidth: 300,
    },
    avatar: {
        margin: '1em',
        textAlign: 'center ',
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        display: 'flex',
    },
};

@inject('auth') @observer
export default class Login extends Component {

    state = {
        login: undefined,
        password: undefined,
        messageType: null,
        message: null,
    };

    handleEmailChange = (e) => {
        this.setState({
            email: e.target.value,
        });
    }

    handlePasswordChange = (e) => {
        this.setState({
            password: e.target.value,
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const emailVal = this.state.email;
        const passVal = this.state.password;

        this.signIn(emailVal, passVal); // eslint-disable-line object-curly-spacing
    }

    async signIn(login, password) { // eslint-disable-line object-curly-spacing
        try {
            await this.props.auth.localLogin(login, password);
        } catch (error) {
            this.setState({
                messageType: 'warning',
                message: error.message,
            });
            if (error.name === 'WrongCredentialsError') {
                this.setState({ fail: true });
            }
        }
    }

    render() {
        const { isLoading, theme, auth } = this.props;
        const { message, messageType } = this.state;
        const muiTheme = getMuiTheme(theme);
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div style={{ ...styles.main, backgroundColor: colors.lighterBlue }}>
                    <Card style={styles.card}>
                        <div style={styles.avatar}>
                            <Avatar backgroundColor={colors.blue} icon={<LockIcon color={colors.blue} />} size={60} />
                        </div>
                        <form onSubmit={this.handleSubmit}>
                            <div style={styles.form}>
                                <div style={styles.input} >
                                    <TextField
                                      fullWidth
                                      name="username"
                                      floatingLabelText="LOGIN: info@jbsolutions.pl"
                                      disabled={isLoading}
                                      onChange={this.handleEmailChange}
                                    />
                                </div>
                                <div style={styles.input}>
                                    <TextField
                                      fullWidth
                                      name="password"
                                      floatingLabelText="PASS: jbsolutions.pl"
                                      type="password"
                                      disabled={isLoading}
                                      onChange={this.handlePasswordChange}
                                    />
                                </div>
                            </div>
                            <CardActions>
                                <RaisedButton
                                  type="submit"
                                  primary
                                  disabled={isLoading}
                                  icon={isLoading && <CircularProgress size={25} thickness={2} />}
                                  label="Log In"
                                  fullWidth
                                />
                            </CardActions>
                        </form>
                    </Card>
                    {messageType === 'warning' && <Notification message={message} type={messageType} />}
                    {auth.loggedIn && <Redirect to={{ pathname: '/', state: { from: '/' } }} />}
                </div>
            </MuiThemeProvider>
        );
    }
}

Login.propTypes = {
    // authClient: PropTypes.func,
    // previousRoute: PropTypes.string,
    isLoading: PropTypes.bool,
    theme: PropTypes.object.isRequired,
    auth: PropTypes.shape({
    }),
    // handleSubmit: PropTypes.func,
    // userLogin: PropTypes.func.isRequired,
};

Login.defaultProps = {
    theme: defaultTheme,
    isLoading: false,
};

// const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 });
