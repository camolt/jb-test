import React from 'react';
import PropTypes from 'prop-types';

const UserInfo = ({ name }) => {
    return (
        <div className="avatar-info">
            <img alt="" src={""} style={{ width: 40, marginRight: 6 }} />
            <p>{name}</p>
        </div>
    );
};


UserInfo.propTypes = {
    name: PropTypes.string,
};

export default UserInfo;
