import { observable, action } from 'mobx';
import Comparator from '../utils/sorting';

export default class AppState {
    @observable auth
    @observable menuOpen
    @observable currentResult
    @observable loader

    constructor(authStore) {
        this.menuOpen = true;
        this.activeLink = '/vatquery';
        this.auth = authStore;
        this.sortBy = '#';
        this.loader = false;
        this.sortDirection = 1;
        this.history = JSON.parse(localStorage.getItem("history")) || [];
        this.currentResult = '';
    }

    async loadHistory() {
        // const { data } = await this.auth.client.get('/v1/repeater/');
        // this.setHistory(data);
    }

    async checkNip(nipVal) {
        console.log('checkNip in appstate: ', nipVal);
        const formdata = new FormData();
        formdata.append('nip', nipVal);
        this.loader = true;
        try {
            const { data } = await this.auth.client.post(`${__API_URL__}/v1/check_vat`, formdata);
            console.log(data);
            this.currentResult = data;
            this.setRecord(data);
        } catch (error) {
            alert(error)
        }
    }

    @action setRecord(data) {
        this.history.push(data)
        localStorage.setItem('history', JSON.stringify(this.history));
        this.loader = false;
    }

    @action toggleMenu() {
        this.menuOpen = !this.menuOpen;
    }

    @action setActiveLink(val) {
        this.activeLink = val;
    }

    @action setMenuVisibility(val) {
        this.menuOpen = val;
    }


    @action toggleSortDirection() {
        // this.sortDirection = this.sortDirection * -1;
        // this.sortHistory(this.sortBy);
    }

    @action setSortColumn(columnTitle) {
        this.sortBy = columnTitle;
    }
}
