import { observable, computed, action } from 'mobx';
import axios from 'axios';
import { isTokenExpired } from './jwtHelper';


class AuthService {
    @observable token

    constructor() {
        this.logout = this.logout.bind(this);
        this.setToken = this.setToken.bind(this);

        this.token = localStorage.getItem('id_token');
    }

    async localLogin(login, password) {
        const formdata = new FormData();
        formdata.append('login', login);
        formdata.append('password', password);

        try {
            const { data } = await axios.post(`${__API_URL__}/v1/get_token`, formdata);
            this.setToken(data.token);
        } catch (error) {
            const { response: { status } } = error;

            if (error.response.headers['x-auth-tfa'] === 'required') {
                throw new Error({
                    name: 'NeedTFAError',
                    message: 'TFA code requested',
                });
            }

            if (status !== 401) {
                throw new Error(error);
            }

            throw new Error({
                name: 'WrongCredentialsError',
                message: 'Wrong email or password',
            });
        }
    }

    @computed get loggedIn() {
        return !!this.token && !isTokenExpired(this.token);
    }

    @computed get client() {
        const client = axios.create({
            baseURL: __API_URL__,
            headers: { Authorization: this.token },
            withCredentials: true,
        });

        // Rude implementation for RFC7396
        // https://tools.ietf.org/html/rfc7396
        client.mergePatch = (url, data, opts) => {
            return client.patch(url, data, Object.assign({}, {
                headers: {
                    'Content-Type': 'application/merge-patch+json',
                },
            }, opts));
        };

        return client;
    }

    @action setToken(idToken) {
        this.token = idToken;
        console.warn('idToken: ', idToken);
        localStorage.setItem('id_token', idToken);
    }

    @action logout() {
        console.warn('logout');
        this.token = undefined;
        localStorage.removeItem('id_token');
    }
}

const store = new AuthService();
export default store;
