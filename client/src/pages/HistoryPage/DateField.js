import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';
import pure from 'recompose/pure';

const toLocaleStringSupportsLocales = (() => {
    // from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
    try {
        new Date().toLocaleString('i');
    } catch (error) {
        return (error instanceof RangeError);
    }
    return false;
})();

export const DateField = ({ elStyle, locales, options, record, showTime = false, source }) => {
    if (!record) return null;
    const value = get(record, source);
    if (value == null) return null;
    const date = value instanceof Date ? value : new Date(value);
    const dateString = showTime ?
        (toLocaleStringSupportsLocales ? date.toLocaleString(locales, options) : date.toLocaleString()) :
        (toLocaleStringSupportsLocales ? date.toLocaleDateString(locales, options) : date.toLocaleDateString());

    return <span style={elStyle}>{dateString}</span>;
};

DateField.propTypes = {
    elStyle: PropTypes.object,
    locales: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
    ]),
    options: PropTypes.object,
    record: PropTypes.object,
    showTime: PropTypes.bool,
    source: PropTypes.string.isRequired,
};

DateField.defaultProps = {
    record: {},
    elStyle: {},
    locales: '',
    options: {},
    showTime: false,
};

const PureDateField = pure(DateField);

export default PureDateField;
