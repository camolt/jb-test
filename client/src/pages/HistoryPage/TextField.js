import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';
import pure from 'recompose/pure';

const TextField = ({ source, record = {}, elStyle }) => {
    return <span style={elStyle}>{get(record, source)}</span>;
};

TextField.propTypes = {
    elStyle: PropTypes.object,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};

TextField.defaultProps = {
    record: {},
    elStyle: {},
};

const PureTextField = pure(TextField);


export default PureTextField;
