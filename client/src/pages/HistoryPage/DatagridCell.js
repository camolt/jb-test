import React from 'react';
import PropTypes from 'prop-types';
import defaultsDeep from 'lodash.defaultsdeep';
import { TableRowColumn } from 'material-ui/Table';

const DatagridCell = ({ className, style, defaultStyle, children }) => {
    const computedStyle = defaultsDeep({}, style, defaultStyle);
    return (
        <TableRowColumn className={className} style={computedStyle}>
            {children}
        </TableRowColumn>
    );
};

DatagridCell.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    defaultStyle: PropTypes.shape({
        td: PropTypes.object,
        'td:first-child': PropTypes.object
    }),
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
    ])
};

export default DatagridCell;
