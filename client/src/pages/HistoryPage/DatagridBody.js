import React from 'react';
import PropTypes from 'prop-types';
import { propTypes } from 'mobx-react';
import { TableBody, TableRow } from 'material-ui/Table';
import LockedIcon from 'material-ui/svg-icons/action/lock';
import UnlockedIcon from 'material-ui/svg-icons/action/lock-open';
import IconButton from 'material-ui/IconButton';
import DatagridCell from './DatagridCell';
import colors from '../../config/variables';

const buttonStyles = {
    border: `1px solid ${colors.lighterBlue}`,
    borderRadius: '5px',
    width: '30px',
    height: '30px',
    padding: 0
};

class DatagridBody extends React.Component {

    render() {
        const { styles, history, filterValue } = this.props; // eslint-disable-line
        const re = new RegExp(filterValue, 'i');


        const historyMarkup = history.map((rec, index) => {
            return (
                <TableRow style={index % 2 === 0 ? styles['tr.even'] : styles.tr}
                          selectable={false} key={index} >
                    <DatagridCell defaultStyle={styles.cell['td:first-child']} >
                        {index + 1}
                    </DatagridCell>
                    <DatagridCell defaultStyle={styles.cell.td} >
                        {rec.requestDate}
                    </DatagridCell>
                    <DatagridCell defaultStyle={styles.cell.td} >
                        {rec.vatNumber }
                    </DatagridCell>
                    <DatagridCell defaultStyle={styles.cell.td} >
                        {rec.valid ? "TAK" : "NIE"}
                    </DatagridCell>
                </TableRow>
            );
        });

        return (
            <TableBody displayRowCheckbox={false} className="datagrid-body" >
                {historyMarkup}
            </TableBody>
        );
    }
}

DatagridBody.propTypes = {
    styles: PropTypes.shape({
    }),
    history: PropTypes.oneOfType([
        propTypes.observableArray,
        PropTypes.array,
    ]).isRequired,
    filterValue: PropTypes.string,
};

// trick material-ui Table into thinking this is one of the child type it supports
const DatagridBodyMod = DatagridBody;
DatagridBodyMod.muiName = 'TableBody';

export default DatagridBodyMod;
