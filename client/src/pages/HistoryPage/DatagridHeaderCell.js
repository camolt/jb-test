import React from 'react';
import PropTypes from 'prop-types';
import { TableHeaderColumn } from 'material-ui/Table';
import SortIcon from 'material-ui/svg-icons/action/code';
import IconButton from 'material-ui/IconButton';
import colors from '../../config/variables';

const DatagridHeaderCell = ({ defaultStyle, title, sortable, sortBy }) => {
    const style = defaultStyle;

    return (
        <TableHeaderColumn style={style}>
            {title}
            {sortable && <IconButton iconStyle={{ height: '12px' }} style={{ transform: 'rotate(90deg)', position: 'absolute', right: 0 }} onClick={() => sortBy(title)} >
                <SortIcon color={colors.darkBlue} />
            </IconButton>}
        </TableHeaderColumn>
    );
};

DatagridHeaderCell.propTypes = {
    defaultStyle: PropTypes.shape({
        th: PropTypes.object,
        'th:first-child': PropTypes.object,
        sortButton: PropTypes.object,
        nonSortableLabel: PropTypes.object
    }),
    title: PropTypes.string,
    sortable: PropTypes.bool,
    sortBy: PropTypes.func
};

export default DatagridHeaderCell;
