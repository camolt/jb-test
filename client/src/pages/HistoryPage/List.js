import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject } from 'mobx-react';
import RaisedButton from 'material-ui/RaisedButton';
import AddIcon from 'material-ui/svg-icons/content/add';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import ViewTitle from '../../components/AppPage/ViewTitle';
import defaultTheme from '../../config/defaultTheme';


const styles = {
    noResults: { padding: 20 },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
    },
};


@inject('store')
export class List extends Component {
    constructor(props) {
        super(props);
        this.state = { key: 0 };
    }

    toggleMenu= () => {
        this.props.store.toggleMenu();
    }

    render() {
        const { children } = this.props;

        const muiTheme = getMuiTheme(defaultTheme);

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div className="list-page">
                    <div style={styles.header} >
                        <ViewTitle title="VAT EU QUERY HISTORY" toggleSidebar={this.toggleMenu} />
                    </div>
                    <div>
                        {children}
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

List.propTypes = {
    children: PropTypes.element,
    store: PropTypes.shape({
        toggleMenu: PropTypes.func,
    }),
};

List.defaultProps = {
    theme: defaultTheme,
};


export default List;
