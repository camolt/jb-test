import React from 'react';
import PropTypes from 'prop-types';
import { observer, propTypes } from 'mobx-react';
import { Table, TableHeader, TableRow } from 'material-ui/Table';
import DatagridHeaderCell from './DatagridHeaderCell';
import DatagridBody from './DatagridBody';
import colors from '../../config/variables';



const defaultStyles = {
    tr: {
        borderColor: colors.lighterBlue
    },
    'tr.even': {
        backgroundColor: '#fff'
    },
    table: {
        tableLayout: 'auto',
        backgroundColor: colors.background
    },
    tbody: {
        height: 'inherit'
    },
    header: {
        th: {
            padding: '0 12px',
            borderLeft: `1px solid ${colors.lighterBlue}`,
            fontSize: '16px',
            fontWeight: '600',
            lineHeight: '48px',
            fontFamily: 'Source Sans Pro',
            color: colors.headerText
        },
        'th:first-child': {
            padding: '0 0 0 12px',
            borderLeft: 0,
            width: '70px'
        }
    },
    cell: {
        td: {
            padding: '0 12px',
            whiteSpace: 'normal',
            borderLeft: '1px solid',
            borderColor: colors.lighterBlue,
            fontFamily: 'Source Sans Pro',
            fontSize: '16px'
        },
        'td:first-child': {
            padding: '0 12px 0 16px',
            whiteSpace: 'normal',
            borderLeft: '0'
        }
    }
};
export { defaultStyles };

const Datagrid = observer(({ history, styles = defaultStyles, sortBy, filterValue }) => {

    return (
        <Table style={styles.table} fixedHeader={false}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false} >
                <TableRow style={{ borderColor: colors.lighterBlue }}>
                    <DatagridHeaderCell defaultStyle={{ ...styles.header.th, ...styles.header['th:first-child'] }} title="#" sortable sortBy={sortBy} />
                    <DatagridHeaderCell defaultStyle={styles.header.th}
                                        title="Date"
                                        sortable sortBy={sortBy} />
                    <DatagridHeaderCell defaultStyle={styles.header.th}
                                        title="NIP Value" />
                    <DatagridHeaderCell defaultStyle={styles.header.th}
                                        title="VAT EU Valid"
                                        sortable sortBy={sortBy} />
                </TableRow>
            </TableHeader>
            <DatagridBody filterValue={filterValue} styles={styles}
                          history={history} />
        </Table>
    );
});

Datagrid.propTypes = {
    styles: PropTypes.object,
    history: PropTypes.oneOfType([
        propTypes.observableArray,
        PropTypes.array,
    ]),
    filterValue: PropTypes.string
};

export default Datagrid;
