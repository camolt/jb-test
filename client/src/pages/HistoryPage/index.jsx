import React from 'react';
import AppPage from '../../components/AppPage/AppPage';
import { inject, observer } from 'mobx-react';
import Protected from '../../components/Protected';
// import TextField from './TextField';
import DateField from './DateField';
import Datagrid from './Datagrid';
import List from './List';

const titleFieldStyle = { maxWidth: '20em', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' };

@Protected
@inject('store')
@observer
export default class HistoryPage extends React.Component {

    constructor(props) {
        super(props);
        this.store = this.props.store;
        this.state = {
            filterValue: ''
        };
    }

    loadHistory = () => {
        this.store.loadHistory();
    }

    componentDidMount() {
        this.loadHistory();
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
    }

    sortBy = (columnTitle) => {
        if (columnTitle === this.store.sortBy) {
            this.store.toggleSortDirection();
        } else {
            this.store.setSortColumn(columnTitle);
        }
    }

    render() {
        return (
            <AppPage >
              <List>
                  <Datagrid
                    filterValue={String(this.state.filterValue)}
                    history={this.store.history}
                    sortBy={this.sortBy} />
              </List>
            </AppPage>
        );
    }
}
