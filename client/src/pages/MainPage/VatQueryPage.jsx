import React, { Component } from 'react';
import AppPage from '../../components/AppPage/AppPage';
import Protected from '../../components/Protected';
import { inject, observer } from 'mobx-react';





@Protected
@inject('store')
@observer
export default class VatQueryPage extends React.Component {

    handleSubmit = (event) => {
        event.preventDefault();
        const nipVal = this.state.nip;
        console.log('handleSubmit in vatquery page: ', nipVal);
        console.log(this.props.store);
        this.props.store.checkNip(nipVal); // eslint-disable-line object-curly-spacing
    }

    nipChanged = (e) => {
        console.log('nipChanged in vatpage: ', e.target.value);
        this.setState({
            nip: e.target.value,
        });
    }

    statusIndicator = (status) => {
        const htmlElements = [];
        const spinnerDots = 12;

        for (let i = 0; i < spinnerDots; i += 1) {
            htmlElements.push(<div key={i} />);
        }

        switch (status) {
        case false:
            return ("");
        default:
            return (<div className="nip-status">
                <div className="loader">
                    {htmlElements}
                </div>
            </div>
            );
        }
    }

    render() {
        return (
            <AppPage searchBar handleSubmit={this.handleSubmit} nipChanged={this.nipChanged}>
                <h2 id="heading">VAT QUERY RESULTS:</h2>
                <div className="resultsContainer">
                    {this.statusIndicator(this.props.store.loader)}
                {Object.keys(this.props.store.currentResult).map((el,idx) => {
                    return (
                        <div key={idx}>
                            <p>{`${el}: ${this.props.store.currentResult[el]}`}</p>
                        </div>
                    )
                })}
                </div>
            </AppPage>
        );
    }
}
