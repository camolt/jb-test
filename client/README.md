# Camo Readme client

Based on [ES6 React boilerplate using Webpack 3 (https://github.com/KleoPetroff/react-webpack-boilerplate).

## Starting the application

Make sure you have Node.js at least version 7, Python 3 installed

1. `git clone git@bitbucket.org:camolt/jb-test.git`
2. `cd client\`
3. Run `npm install` or `yarn install`
4. Start webpack development server using `npm start` or `npm run dashboard` for webpack dashboard
5. Start backend server using instructions from `api/` folder
6. Open [http://127.0.0.1:8080](http://127.0.0.1:8080)
7. Log in with following credentials:
    login: `info@jbsolutions.pl`
    password: `jbsolutions`

## Available Commands

- `npm start` - start the dev server
- `npm run dashboard` - start the dev server with webpack fancy dashboard
- `npm run production` - create a production ready build in `dist` folder
- `npm run lint` - execute an eslint check
- `npm run lint-fix` - execute an eslint check and do the fixes automatically
- `npm test` - run all tests

## Vendor Exporting

You can export specific vendors in separate files and load them. All vendors should be included in `app/vendors` and will be exported in a `vendors` folder under `dist`. The main idea is to serve independent JavaScript and CSS libraries, though currently all file formats are supported.

! Don't forget to add the vendors in `app/index.html` and `build/index.html`.

## Production code

Run `npm run production`. The production-ready code will be located under `dist` folder.

# Troubleshooting
